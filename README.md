# README #

Movie Finder app using https://developer.spotify.com/web-api/search-item/

## Prerequisites

- [node.js](http://nodejs.org) Node 6 with NPM 3 is required.
- [gulp](http://gulpjs.com/) `npm install -g gulp`
- [git](https://git-scm.com/downloads) git cmd tool is required


#Start Development

* run npm install
* run gulp
* Go to dist folder & open index.html in your favorite browser