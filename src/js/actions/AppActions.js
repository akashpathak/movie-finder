var AppDispatcher = require('../dispatcher/AppDispatcher');
//var AppConstants = require('../constants/AppConstants');

var AppAction = {
    searchMovies: function(movie){
        AppDispatcher.handleViewAction({
            actionType:'SEARCH_MOVIES',
            movie:movie
        });
    },
    filterSelect:function(filter){
        AppDispatcher.handleViewAction({
            actionType:'FILTER_SELECT',
            filter:filter
        });
    },
    receiveMovieResults:function(movies_data){
        AppDispatcher.handleViewAction({
            actionType:'RECEIVE_MOVIE_RESULTS',
            movies:movies_data
        });
    }

};

module.exports = AppAction;