var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter =  require('events').EventEmitter;
var assign = require('object-assign');
var AppAPI = require('../utils/AppAPI.js');
var CHANGE_EVENT = 'change';

var _movies = [];
var _selected = '';


var AppStore = assign({},EventEmitter.prototype, {

     //Adding listeners
    setMovieResults: function(movie){
        _movies = movie
    },
    getMovieResults:function(){
      return _movies
    },
    emitChange:function(){
        this.emit(CHANGE_EVENT);
    },
    addChangeListener:function(callback){
        this.on('change', callback);
    },
    removeChangeListener: function(callback){
        this.removeListener('change', callback);

    }
});

AppDispatcher.register(function(payload){
    var action = payload.action;
    switch (action.actionType){
        case 'SEARCH_MOVIES':
            console.log('Searching from store' + action.movie.title);
            AppStore.emit(CHANGE_EVENT);
            AppAPI.searchMovies(action.movie);
            break;
        case 'FILTER_SELECT':
            AppStore.emit(CHANGE_EVENT);
            AppAPI.filterSelect(action.filter);
            console.log("Selected " + action.filter);
            break;
        case 'RECEIVE_MOVIE_RESULTS':
            AppStore.setMovieResults(action.movies);
            AppStore.emit(CHANGE_EVENT);
            break;
    }
    return true;
})

module.exports = AppStore;