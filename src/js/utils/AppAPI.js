var AppAction = require('../actions/AppActions');
var filterSelected = 'album'
var AddingS =   's';

module.exports= {
    filterSelect:function(filter){
        filterSelected = filter;
    },
    searchMovies: function(movies){
        $.ajax({
            url: 'https://api.spotify.com/v1/search?q=' + movies.title + '&type='+filterSelected,
            datatype: 'json',
            cache: true,
            success: function (data) {
                var PluralFilterSelected = filterSelected.concat(AddingS);
                AppAction.receiveMovieResults(data[PluralFilterSelected]);
            }.bind(this),
            error: function (xhr, status, err) {
                alert(err);
            }.bind(this)
        });
    }
};
