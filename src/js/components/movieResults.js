/**
 * Created by Akash on 05/11/16.
 */
/**
 * Created by Akash on 05/11/16.
 */
var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore.js');
var Movie = require('./Movie.js')


var MovieResults = React.createClass({
    render: function () {
        return (
            <div className="container">
                <div className="col-xs-12 col-sm-8 col-sm-offset-2">
                    {
                        this.props.movies.map(function(movie,i){
                            return (
                                <Movie movie={movie} key={i} />
                            )
                        })
                    }
                    </div>
             </div>
         )
    }
});
module.exports = MovieResults;
