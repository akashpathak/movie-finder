var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore.js');
var SearchForm = require('./SearchForm');
var MovieResults = require('./MovieResults');
var NavBar = require('./NavBar');

function getAppState(){
    return {
        movies: AppStore.getMovieResults()
    }
}

var App = React.createClass({

    getInitialState:function(){
        return getAppState();
    },
    componentDidMount: function () {
        AppStore.addChangeListener(this._onChange);
    },
    componentWillMount: function(){
        AppStore.removeChangeListener(this._onChange)
    },

   render:function(){
        if(this.state.movies == ''){
            var movieResults = '';
            var counterTotal = '';
        }
       else {
            var movieResults = <MovieResults movies={$.makeArray(this.state.movies)}/>;
            var counterTotal = this.state.movies;
            console.log(counterTotal);
            console.log(this.state)

        }

       return(
           <div>
               <NavBar counter={counterTotal}/>
               <SearchForm loading={this.state.loading}/>
               {movieResults}
           </div>
       )
   },

    _onChange:function(){
        this.setState(getAppState());
    }
});
module.exports = App;
