/**
 * Created by Akash on 05/11/16.
 */
var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore.js');

var NavBar = React.createClass({
    getInitialState:function(){
        return {selectValue:''};
    },
    handleChange:function(e){
        this.setState({selectValue:e.target.value});
        e.preventDefault();
        AppActions.filterSelect(this.state.selectValue);
    },
    render: function () {
        return (
            <nav className="navbar navbar-default">
                <div className="container-fluid">
                    <div className="container">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span className="sr-only">Toggle navigation</span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <div className="navbar-brand" href="#"><i className="fa fa-rebel" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav">
                                <li className="dropdown">
                                    <select
                                        className="form-control custom-select"
                                        defaultValue={this.state.selectValue}
                                        onChange={this.handleChange}
                                        >
                                        <option value="album">Album</option>
                                        <option value="artist">Artist</option>
                                        <option value="playlist">Playlist</option>
                                        <option value="track">Track</option>
                                    </select>
                                </li>
                            </ul>
                            <ul className="nav navbar-nav navbar-right">
                                {
                                    $.makeArray(this.props.counter).map(function(movie,i){
                                            return (
                                                <li className="counter" key={i}><span className="badge">{movie.total}</span> Counter</li>
                                            )
                                        })
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>

        )
    }

});
module.exports = NavBar;