/**
 * Created by Akash on 05/11/16.
 */
/**
 * Created by Akash on 05/11/16.
 */
/**
 * Created by Akash on 05/11/16.
 */
var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore.js');


var Movie = React.createClass({
    render: function () {
    console.log(this.props.movie.total);
    console.log(this.props.movie.items);

        return (
                <div className="media-result">
                            {
                                this.props.movie.items.map(function (data, i) {
                                    return (
                                            <div className="media" key={i}>
                                            <div className="media-left">
                                                <a href={data.external_urls.spotify}>
                                                 {
                                                     $.makeArray(data.images[1]).map(function(data,i){
                                                        return(
                                                            <img className="media-object thumb" src={data.url==""? 'http://placekitten.com/64/64': data.url} key={i} width="64" height="64"/>
                                                            )
                                                    })
                                                 }
                                                </a>
                                                </div>
                                                <div className="media-body">
                                                    <h4 className="media-heading">{data.name}</h4>
                                                    {data.type}
                                                </div>
                                            </div>
                                    )
                                })
                            }

                </div>

        )
    }
});
module.exports = Movie;
