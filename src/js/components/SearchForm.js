/**
 * Created by Akash on 05/11/16.
 */
var React = require('react');
var ReactDOM = require('react-dom');
var AppActions = require('../actions/AppActions');
var AppStore = require('../stores/AppStore.js');


var SearchForm = React.createClass({
    getInitialState: function() {
        return {
            FilterEmpty: false
        }
    },
    render: function () {
        return (
            <div className="container">
                <form className="form-horizontal" onSubmit={this.onSubmit}>
                    <div className="form-group form-group-lg">
                        <div className="col-xs-12 col-sm-8 col-sm-offset-2">
                            <input className="form-control" type="text" id="formGroupInputLarge" ref="title"
                                   placeholder="Search Hitman... " required />
                            <a className="search-icon" onClick={this.onSubmit}><i className="fa fa-search" aria-hidden="true"></i></a>
                            <p className={!this.state.FilterEmpty? 'hidden':'error'} >Please fill out the form.</p>
                        </div>
                    </div>
                </form>
            </div>
        )
    },

    onSubmit: function (e) {
        e.preventDefault();
        if(this.refs.title.value == ''){
            this.setState({FilterEmpty: true});
            return false;
        }
        else{
            var movie = {
                title: this.refs.title.value.trim()
            }
            AppActions.searchMovies(movie);
            this.setState({FilterEmpty: false});

        }

    },
});
module.exports = SearchForm;
